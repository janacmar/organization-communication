<?php

namespace App\Form;

use App\Entity\Communication;
use App\Entity\Employee;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** Form for creating and editing communications. */
class CommunicationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', TextType::class, ['label' => 'Type*'])
            ->add('subject', TextType::class, ['label' => 'Subject*'])
            ->add('from', DateTimeType::class, ['label' => 'From*', 'years' => range(1990, 2040)])
            ->add('to', DateTimeType::class, [
                'label' => 'To',
                'required' => false,
                'years' => range(1990,2040)
            ])
            ->add('summary', TextareaType::class, ['label' => 'Summary', 'required' => false])
            ->add('participations', CollectionType::class, [
                'entry_type' => CommunicationParticipationType::class,
                'entry_options' => ['label' => false],
                'label' => 'Participating persons',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'data' => $options['participations']
            ])
            ->add('employees', EntityType::class, [
                'label' => 'Authorize employees to communication:',
                'class' => Employee::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                              ->orderBy('e.surname', 'ASC')
                              ->addOrderBy('e.name', 'ASC');
                },
                'choice_label' => function (Employee $employee) {
                    return $employee->getSurname().', '.$employee->getName();
                },
                'multiple' => true,
                'expanded' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Communication::class,
            'participations' => []
        ]);
    }
}
