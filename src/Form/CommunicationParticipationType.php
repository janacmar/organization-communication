<?php

namespace App\Form;

use App\Entity\Participation;
use App\Entity\Person;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** Form for creating and editing persons' participation in communication. */
class CommunicationParticipationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('person', EntityType::class, [
                'class' => Person::class,
                'label' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                              ->orderBy('p.surname', 'ASC')
                              ->addOrderBy('p.name', 'ASC');
                },
                'choice_label' => function (Person $person) {
                    return $person->getSurname().', '.$person->getName();
                }
            ])
            ->add('role', TextType::class, ['label' => 'Role in communication', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participation::class,
        ]);
    }
}
