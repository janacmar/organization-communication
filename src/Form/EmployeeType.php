<?php

namespace App\Form;

use App\Entity\Employee;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** Form for creating and editing employees. */
class EmployeeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('person', PersonType::class, [
                'data_class' => Employee::class,
                'access' => $options['access'],
                'participations' => $options['participations']
            ])
        ;
        // only user with access can edit following properties
        if ($options['access']) {
            $builder
                ->add('wage', TextType::class, ['label' => 'Wage', 'required' => false])
                ->add('account', TextType::class, ['label' => 'Bank account', 'required' => false])
                ->add('hired', DateType::class, [
                    'label' => 'Hiring date',
                    'required' => false,
                    'years' => range(1990,2040)
                ])
                ->add('fired', DateType::class, [
                    'label' => 'Termination date',
                    'required' => false,
                    'years' => range(1990,2040)
                ])
                ->add('username', TextType::class, ['label' => 'Username*'])
                ->add('password', PasswordType::class, ['label' => 'Password*'])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Employee::class,
            'access' => true,
            'participations' => []
        ]);
    }
}
