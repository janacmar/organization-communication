<?php

namespace App\Form;

use App\Entity\Communication;
use App\Entity\Participation;
use App\Functionality\CommonFunctionality;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/** Form for creating and editing person's participation in communications. */
class PersonParticipationType extends AbstractType
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /** @var CommonFunctionality */
    protected $commonFunctionality;

    /**
     * @param EntityManagerInterface $entityManager
     * @param CommonFunctionality $commonFunctionality
     */
    public function __construct(EntityManagerInterface $entityManager, CommonFunctionality $commonFunctionality)
    {
        $this->entityManager = $entityManager;
        $this->commonFunctionality = $commonFunctionality;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $communications = $this->entityManager->getRepository(Communication::class)->findAll();
        $communications = $this->commonFunctionality->accessibleCommunicationFilter($communications);
        $communications = $this->commonFunctionality->sortCommunicationsByDate($communications);
        if (! $communications) {
            return;
        }
        $builder
            ->add('communication', EntityType::class, [
                'class' => Communication::class,
                'label' => false,
                'choices' => $communications,
                'choice_label' => 'subject'
            ])
            ->add('role', TextType::class, ['label' => 'Role in communication', 'required' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Participation::class,
        ]);
    }
}