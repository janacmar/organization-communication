<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Image;

/** Form for creating and editing properties common to all person subtypes. */
class PersonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('participations', CollectionType::class, [
                'entry_type' => PersonParticipationType::class,
                'entry_options' => ['label' => false],
                'label' => 'Communication participation',
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'data' => $options['participations']
            ])
        ;
        // only user with access can edit following properties
        if ($options['access']) {
            $builder
                ->add('name', TextType::class, ['label' => 'Name*'])
                ->add('surname', TextType::class, ['label' => 'Surname*'])
                ->add('street', TextType::class, ['label' => 'Street', 'required' => false])
                ->add('house', TextType::class, ['label' => 'House number', 'required' => false])
                ->add('city', TextType::class, ['label' => 'City', 'required' => false])
                ->add('zip', TextType::class, ['label' => 'ZIP Code', 'required' => false])
                ->add('country', TextType::class, ['label' => 'Country', 'required' => false])
                ->add('phone', TelType::class, ['label' => 'Phone', 'required' => false])
                ->add('email', EmailType::class, ['label' => 'Email', 'required' => false])
                ->add('photo', FileType::class, [
                    'label' => 'Photo',
                    'required' => false,
                    'mapped' => false,
                    'constraints' => new Image()
                ])
                ->add('description', TextareaType::class, ['label' => 'Description', 'required' => false])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'inherit_data' => true,
            'access' => true,
            'participations' => []
        ]);
    }
}
