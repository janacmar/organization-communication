<?php

namespace App\Repository;

use App\Entity\Accessible;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Accessible|null find($id, $lockMode = null, $lockVersion = null)
 * @method Accessible|null findOneBy(array $criteria, array $orderBy = null)
 * @method Accessible[]    findAll()
 * @method Accessible[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccessibleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Accessible::class);
    }

    // /**
    //  * @return Accessible[] Returns an array of Accessible objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Accessible
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
