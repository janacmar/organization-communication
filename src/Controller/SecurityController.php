<?php

namespace App\Controller;

use App\Entity\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/** Controls requests related to user authentication. */
class SecurityController extends AbstractController
{
    /**
     * Logs user in.
     *
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // redirects already logged user to his communications
        if ($this->getUser()) {
            /** @var Employee $employee */
            $employee = $this->getUser();
            return $this->redirectToRoute('person_communications', ['id' => $employee->getId()]);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * Logs user out.
     *
     * @Route("/logout", name="app_logout", methods={"GET"})
     * @throws \LogicException
     */
    public function logout()
    {
        // if correctly configured, this code can't be reached
        throw new \LogicException('Don\'t forget to activate logout in security.yaml');
    }
}
