<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Form\EmployeeType;
use App\Functionality\EmployeeFunctionality;
use App\Functionality\PersonFunctionality;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controls requests related to employees.
 *
 * @Route("/employee")
 */
class EmployeeController extends AbstractController
{
    /** @var PersonFunctionality */
    protected $personFunctionality;

    /** @var EmployeeFunctionality */
    protected $employeeFunctionality;

    /**
     * @param PersonFunctionality $personFunctionality
     * @param EmployeeFunctionality $employeeFunctionality
     */
    public function __construct(PersonFunctionality $personFunctionality, EmployeeFunctionality $employeeFunctionality)
    {
        $this->personFunctionality = $personFunctionality;
        $this->employeeFunctionality = $employeeFunctionality;
    }

    /**
     * Creates new employee.
     *
     * @Route("/create", name="employee_create")
     * @param Request $request
     * @return Response
     */
    public function createAction (Request $request)
    {
        $employee = new Employee();
        $form = $this->createForm(EmployeeType::class, $employee);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $photo = $form->get('person')->get('photo')->getData();
            $this->personFunctionality->uploadPersonPhoto($photo, $employee);
            $this->employeeFunctionality->save($employee);
            return $this->redirectToRoute('employee_detail', ['id' => $employee->getId()]);
        }
        return $this->render('employee/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Edits employee. Common user can edit only communication participation, employee manager can edit everything.
     *
     * @Route("/{id}/edit", name="employee_edit", requirements={"id"="\d+"})
     * @param Employee $employee
     * @param Request $request
     * @return Response
     */
    public function editAction (Employee $employee, Request $request)
    {
        $access = $this->isGranted('ROLE_EMPLOYEE_MANAGER');
        $participations = $this->personFunctionality->accessibleParticipationFilter($employee->getParticipations());
        $participations = $this->personFunctionality->sortParticipationsByDate($participations);
        $form = $this->createForm(EmployeeType::class, $employee, [
            'access' => $access,
            'participations' => $participations,
        ]);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            if ($access) {
                $photo = $form->get('person')->get('photo')->getData();
                $this->personFunctionality->uploadPersonPhoto($photo, $employee);
            }
            $this->employeeFunctionality->save($employee);
            return $this->redirectToRoute('employee_detail', ['id' => $employee->getId()]);
        }
        return $this->render('employee/edit.html.twig', ['form' => $form->createView(), 'employee' => $employee]);
    }

    /**
     * Deletes employee.
     *
     * @Route("/{id}/delete", name="employee_delete", requirements={"id"="\d+"})
     * @param Employee $employee
     * @return Response
     * @throws AccessDeniedHttpException
     */
    public function deleteAction (Employee $employee)
    {
        if ($this->getUser() === $employee) {
            throw new AccessDeniedHttpException();
        }
        $this->employeeFunctionality->delete($employee);
        return $this->redirectToRoute('employee_list');

    }

    /**
     * Displays employee's profile.
     *
     * @Route("/{id}/detail", name="employee_detail", requirements={"id"="\d+"})
     * @param Employee $employee
     * @return Response
     */
    public function detailAction (Employee $employee)
    {
        return $this->render("employee/detail.html.twig", ['employee' => $employee]);
    }

    /**
     * Displays list of all employees.
     *
     * @Route("/list", name="employee_list")
     * @return Response
     */
    public function listAction ()
    {
        $employees = $this->getDoctrine()->getRepository(Employee::class)->findBy([], [
            'surname' => 'ASC',
            'name' => 'ASC'
        ]);
        return $this->render("employee/list.html.twig", ['employees' => $employees]);
    }
}