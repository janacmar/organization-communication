<?php

namespace App\Controller;

use App\Entity\Employee;
use App\Entity\Person;
use App\Form\ExternalPersonType;
use App\Functionality\ExternalPersonFunctionality;
use App\Functionality\PersonFunctionality;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controls requests related to external persons (persons who are not employees).
 *
 * @Route("/externalPerson")
 */
class ExternalPersonController extends AbstractController
{
    /** @var PersonFunctionality */
    protected $personFunctionality;

    /** @var ExternalPersonFunctionality */
    protected $externalPersonFunctionality;

    /**
     * @param PersonFunctionality $personFunctionality
     * @param ExternalPersonFunctionality $externalPersonFunctionality
     */
    public function __construct(PersonFunctionality $personFunctionality, ExternalPersonFunctionality $externalPersonFunctionality)
    {
        $this->personFunctionality = $personFunctionality;
        $this->externalPersonFunctionality = $externalPersonFunctionality;
    }

    /**
     * Creates new external person.
     *
     * @Route("/create", name="externalPerson_create")
     * @param Request $request
     * @return Response
     */
    public function createAction (Request $request)
    {
        $person = new Person();
        $form = $this->createForm(ExternalPersonType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $photo = $form->get('person')->get('photo')->getData();
            $this->personFunctionality->uploadPersonPhoto($photo, $person);
            $this->externalPersonFunctionality->save($person);
            return $this->redirectToRoute('externalPerson_detail', ['id' => $person->getId()]);
        }
        return $this->render('externalPerson/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Edits external person.
     *
     * @Route("/{id}/edit", name="externalPerson_edit", requirements={"id"="\d+"})
     * @param Person $person
     * @param Request $request
     * @return Response
     * @throws NotFoundHttpException
     */
    public function editAction (Person $person, Request $request)
    {
        if ($person instanceof Employee) {
            throw $this->createNotFoundException();
        }
        $participations = $this->personFunctionality->accessibleParticipationFilter($person->getParticipations());
        $participations = $this->personFunctionality->sortParticipationsByDate($participations);
        $form = $this->createForm(ExternalPersonType::class, $person, ['participations' => $participations]);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $photo = $form->get('person')->get('photo')->getData();
            $this->personFunctionality->uploadPersonPhoto($photo, $person);
            $this->externalPersonFunctionality->save($person);
            return $this->redirectToRoute('externalPerson_detail', ['id' => $person->getId()]);
        }
        return $this->render('externalPerson/edit.html.twig', ['form' => $form->createView(), 'person' => $person]);
    }

    /**
     * Deletes external person.
     *
     * @Route("/{id}/delete", name="externalPerson_delete", requirements={"id"="\d+"})
     * @param Person $person
     * @return Response
     * @throws NotFoundHttpException
     */
    public function deleteAction (Person $person)
    {
        if ($person instanceof Employee) {
            throw $this->createNotFoundException();
        }
        $this->externalPersonFunctionality->delete($person);
        return $this->redirectToRoute('externalPerson_list');
    }

    /**
     * Displays external person's profile.
     *
     * @Route("/{id}/detail", name="externalPerson_detail", requirements={"id"="\d+"})
     * @param Person $person
     * @return Response
     * @throws NotFoundHttpException
     */
    public function detailAction (Person $person)
    {
        if ($person instanceof Employee) {
            throw $this->createNotFoundException();
        }
        return $this->render("externalPerson/detail.html.twig", ['person' => $person]);
    }

    /**
     * Displays list of all external persons.
     *
     * @Route("/list", name="externalPerson_list")
     * @return Response
     */
    public function listAction ()
    {
        $persons = $this->getDoctrine()->getRepository(Person::class)->findBy([], [
            'surname' => 'ASC',
            'name' => 'ASC'
        ]);
        $persons = $this->personFunctionality->externalPersonFilter($persons);
        return $this->render("externalPerson/list.html.twig", ['persons' => $persons]);
    }
}