<?php

namespace App\Controller;

use App\Entity\Person;
use App\Functionality\PersonFunctionality;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controls requests related to persons. But only those requests common to all person subtypes.
 *
 * @Route("/person")
 */
class PersonController extends AbstractController
{
    /** @var PersonFunctionality */
    protected $personFunctionality;

    /** @param PersonFunctionality $personFunctionality */
    public function __construct(PersonFunctionality $personFunctionality)
    {
        $this->personFunctionality = $personFunctionality;
    }

    /**
     * Displays list of person's communications accessible by user.
     *
     * @Route("/{id}/communications", name="person_communications", requirements={"id"="\d+"})
     * @param Person $person
     * @return Response
     */
    public function communicationsAction (Person $person)
    {
        $participations = $this->personFunctionality->accessibleParticipationFilter($person->getParticipations());
        $participations = $this->personFunctionality->sortParticipationsByDate($participations);
        return $this->render("person/communications.html.twig", ['person' => $person, 'participations' => $participations]);
    }
}