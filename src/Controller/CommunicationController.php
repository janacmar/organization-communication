<?php

namespace App\Controller;

use App\Entity\Communication;
use App\Form\CommunicationType;
use App\Functionality\CommonFunctionality;
use App\Functionality\CommunicationFunctionality;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controls requests related to communications.
 *
 * @Route("/communication")
 */
class CommunicationController extends AbstractController
{
    /** @var CommonFunctionality */
    protected $commonFunctionality;

    /** @var CommunicationFunctionality */
    protected $communicationFunctionality;

    /**
     * @param CommonFunctionality $commonFunctionality
     * @param CommunicationFunctionality $communicationFunctionality
     */
    public function __construct(CommonFunctionality $commonFunctionality, CommunicationFunctionality $communicationFunctionality)
    {
        $this->commonFunctionality = $commonFunctionality;
        $this->communicationFunctionality = $communicationFunctionality;
    }

    /**
     * Creates new communication.
     *
     * @Route("/create", name="communication_create")
     * @param Request $request
     * @return Response
     */
    public function createAction (Request $request)
    {
        $communication = new Communication();
        $form = $this->createForm(CommunicationType::class, $communication);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $this->communicationFunctionality->save($communication);
            if (! $this->isGranted('access', $communication)) {
                return $this->redirectToRoute('communication_list');
            }
            return $this->redirectToRoute('communication_detail', ['id' => $communication->getId()]);
        }
        return $this->render('communication/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Edits communication.
     *
     * @Route("/{id}/edit", name="communication_edit", requirements={"id"="\d+"})
     * @param Communication $communication
     * @param Request $request
     * @return Response
     */
    public function editAction (Communication $communication, Request $request)
    {
        $this->denyAccessUnlessGranted('access', $communication);
        $participations = $this->communicationFunctionality->sortParticipationsByName($communication->getParticipations());
        $form = $this->createForm(CommunicationType::class, $communication, ['participations' => $participations]);
        $form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid()) {
            $this->communicationFunctionality->save($communication);
            if (! $this->isGranted('access', $communication)) {
                return $this->redirectToRoute('communication_list');
            }
            return $this->redirectToRoute('communication_detail', ['id' => $communication->getId()]);
        }
        return $this->render('communication/edit.html.twig', ['form' => $form->createView(), 'communication' => $communication]);
    }

    /**
     * Deletes communication.
     *
     * @Route("/{id}/delete", name="communication_delete", requirements={"id"="\d+"})
     * @param Communication $communication
     * @return Response
     */
    public function deleteAction (Communication $communication)
    {
        $this->denyAccessUnlessGranted('access', $communication);
        $this->communicationFunctionality->delete($communication);
        return $this->redirectToRoute('communication_list');
    }

    /**
     * Displays communication's profile.
     *
     * @Route("/{id}/detail", name="communication_detail", requirements={"id"="\d+"})
     * @param Communication $communication
     * @return Response
     */
    public function detailAction (Communication $communication)
    {
        $this->denyAccessUnlessGranted('access', $communication);
        $participations = $this->communicationFunctionality->sortParticipationsByName($communication->getParticipations());
        $employeeParticipations = $this->communicationFunctionality->employeeFilter($participations);
        $externalPersonParticipations = $this->communicationFunctionality->externalPersonFilter($participations);
        return $this->render("communication/detail.html.twig", [
            'communication' => $communication,
            'employeeParticipations' => $employeeParticipations,
            'externalPersonParticipations' => $externalPersonParticipations
        ]);
    }

    /**
     * Displays list of all communications accessible by user.
     *
     * @Route("/list", name="communication_list")
     * @return Response
     */
    public function listAction ()
    {
        $communications = $this->getDoctrine()->getRepository(Communication::class)->findAll();
        $communications = $this->commonFunctionality->accessibleCommunicationFilter($communications);
        $communications = $this->commonFunctionality->sortCommunicationsByDate($communications);
        return $this->render("communication/list.html.twig", ['communications' => $communications]);
    }

    /**
     * Displays list of all employees authorized to communication.
     *
     * @Route("/{id}/employees", name="communication_employees", requirements={"id"="\d+"})
     * @param Communication $communication
     * @return Response
     */
    public function employeesAction (Communication $communication)
    {
        $this->denyAccessUnlessGranted('access', $communication);
        $employees = $this->communicationFunctionality->getAllAuthorizedEmployees($communication);
        $employees = $this->communicationFunctionality->sortEmployeesByName($employees);
        return $this->render("communication/employees.html.twig", ['communication' => $communication, 'employees' => $employees]);
    }
}