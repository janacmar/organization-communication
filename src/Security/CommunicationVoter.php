<?php

namespace App\Security;

use App\Entity\Communication;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

/** Decides if user has access to specific communication. */
class CommunicationVoter extends Voter
{
    /** @var Security $security */
    private $security;

    /** @param Security $security */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        if ($attribute != 'access') {
            return false;
        }
        if (! $subject instanceof Communication) {
            return false;
        }
        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        /** @var Communication $communication */
        $communication = $subject;

        $user = $token->getUser();
        if ($this->security->isGranted('ROLE_COMMUNICATION_WATCHER')) {
            return true;
        }
        $access = false;
        // grant access if user is manually authorized to communication
        foreach ($communication->getEmployees() as $employee) {
            if ($employee->getId() === $user->getId()) {
                $access = true;
            }
        }
        //grant access if user participates in communication
        foreach ($communication->getParticipations() as $participation) {
            if ($participation->getPerson()->getId() === $user->getId()) {
                $access = true;
            }
        }
        return $access;
    }
}