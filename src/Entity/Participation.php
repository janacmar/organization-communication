<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Person's participation in communication.
 *
 * @ORM\Entity(repositoryClass="App\Repository\ParticipationRepository")
 */
class Participation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Person's role in communication.
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    private $role;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Communication", inversedBy="participations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $communication;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Person", inversedBy="participations")
     * @ORM\JoinColumn(nullable=false)
     */
    private $person;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getCommunication(): ?Communication
    {
        return $this->communication;
    }

    public function setCommunication(?Communication $communication): self
    {
        $this->communication = $communication;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }
}
