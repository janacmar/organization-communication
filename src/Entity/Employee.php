<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Member of organization which uses this software. Inherits from Person and Person inherits from Subject. Class
 * Table Inheritance used. Employee is also User Entity which can be authenticated in application by username and
 * password. Employee can access Accessible to which he is authorized automatically or manually by other employees.
 *
 * @ORM\Entity(repositoryClass="App\Repository\EmployeeRepository")
 * @UniqueEntity("username")
 */
class Employee extends Person implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * A visual identifier that represents this user.
     *
     * @ORM\Column(type="string", length=128, unique=true)
     * @Assert\Length(max=128)
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * The hashed password.
     *
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * User's roles used to authorization. Set manually by developer for now.
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max=32)
     */
    protected $wage;

    /**
     * Bank account.
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max=64)
     */
    protected $account;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $hired;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $fired;

    /**
     * Accessibles to which employee is manually authorized.
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Accessible", mappedBy="employees")
     */
    protected $accessibles;

    public function __construct()
    {
        parent::__construct();
        $this->accessibles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getWage(): ?string
    {
        return $this->wage;
    }

    public function setWage(?string $wage): self
    {
        $this->wage = $wage;

        return $this;
    }

    public function getAccount(): ?string
    {
        return $this->account;
    }

    public function setAccount(?string $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getHired(): ?\DateTimeInterface
    {
        return $this->hired;
    }

    public function setHired(?\DateTimeInterface $hired): self
    {
        $this->hired = $hired;

        return $this;
    }

    public function getFired(): ?\DateTimeInterface
    {
        return $this->fired;
    }

    public function setFired(?\DateTimeInterface $fired): self
    {
        $this->fired = $fired;

        return $this;
    }

    /** @return Collection|Accessible[] */
    public function getAccessibles(): Collection
    {
        return $this->accessibles;
    }

    public function addAccessible(Accessible $accessible): self
    {
        if (!$this->accessibles->contains($accessible)) {
            $this->accessibles[] = $accessible;
            $accessible->addEmployee($this);
        }

        return $this;
    }

    public function removeAccessible(Accessible $accessible): self
    {
        if ($this->accessibles->contains($accessible)) {
            $this->accessibles->removeElement($accessible);
            $accessible->removeEmployee($this);
        }

        return $this;
    }
}
