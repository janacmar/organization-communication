<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Entity from which person entities inherit. Employee inherits from Person and Person from Subject. Class Table
 * Inheritance used. Persons can participate in communications.
 *
 * @ORM\Entity(repositoryClass="App\Repository\PersonRepository")
 */
class Person extends Subject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max="64")
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max="64")
     * @Assert\NotBlank()
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    protected $city;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    protected $street;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max="16")
     */
    protected $house;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     * @Assert\Length(max="16")
     */
    protected $zip;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    protected $country;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Length(max="32")
     */
    protected $phone;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Assert\Length(max="64")
     */
    protected $email;

    /** @ORM\Column(type="text", nullable=true) */
    protected $description;

    /**
     * Person's participation in communications.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Participation", mappedBy="person",
     *                cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $participations;

    /**
     * Path to photo in images folder.
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $photo;

    public function __construct()
    {
        $this->participations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getHouse(): ?string
    {
        return $this->house;
    }

    public function setHouse(?string $house): self
    {
        $this->house = $house;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(?string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /** @return Collection|Participation[] */
    public function getParticipations(): Collection
    {
        return $this->participations;
    }

    public function addParticipation(Participation $participation): self
    {
        if (!$this->participations->contains($participation)) {
            $this->participations[] = $participation;
            $participation->setPerson($this);
        }

        return $this;
    }

    public function removeParticipation(Participation $participation): self
    {
        if ($this->participations->contains($participation)) {
            $this->participations->removeElement($participation);
            // set the owning side to null (unless already changed)
            if ($participation->getPerson() === $this) {
                $participation->setPerson(null);
            }
        }

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }
}
