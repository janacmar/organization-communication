<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Communication among persons. Inherits from Accessible. Class Table Inheritance used.
 *
 * @ORM\Entity(repositoryClass="App\Repository\CommunicationRepository")
 */
class Communication extends Accessible
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     * @Assert\NotBlank()
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=64)
     * @Assert\Length(max=64)
     * @Assert\NotBlank()
     */
    protected $subject;

    /**
     * @ORM\Column(name="`from`", type="datetime")
     * @Assert\DateTime()
     * @Assert\NotBlank()
     */
    protected $from;

    /**
     * @ORM\Column(name="`to`", type="datetime", nullable=true)
     * @Assert\DateTime()
     */
    protected $to;

    /** @ORM\Column(type="text", nullable=true) */
    protected $summary;

    /**
     * Persons' participation in communication.
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Participation", mappedBy="communication",
     *                cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $participations;

    public function __construct()
    {
        parent::__construct();
        $this->participations = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getFrom(): ?\DateTimeInterface
    {
        return $this->from;
    }

    public function setFrom(\DateTimeInterface $from): self
    {
        $this->from = $from;

        return $this;
    }

    public function getTo(): ?\DateTimeInterface
    {
        return $this->to;
    }

    public function setTo(?\DateTimeInterface $to): self
    {
        $this->to = $to;

        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /** @return Collection|Participation[] */
    public function getParticipations(): Collection
    {
        return $this->participations;
    }

    public function addParticipation(Participation $participation): self
    {
        if (!$this->participations->contains($participation)) {
            $this->participations[] = $participation;
            $participation->setCommunication($this);
        }

        return $this;
    }

    public function removeParticipation(Participation $participation): self
    {
        if ($this->participations->contains($participation)) {
            $this->participations->removeElement($participation);
            // set the owning side to null (unless already changed)
            if ($participation->getCommunication() === $this) {
                $participation->setCommunication(null);
            }
        }

        return $this;
    }
}
