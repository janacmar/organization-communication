<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Entity from which person and group entities inherit. Employee inherits from Person and Person from Subject for
 * now. Class Table Inheritance used.
 *
 * @ORM\Entity(repositoryClass="App\Repository\SubjectRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"subject"="Subject", "person"="Person", "employee"="Employee"})
 */
class Subject
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
