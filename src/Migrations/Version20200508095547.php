<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200508095547 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE accessible (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, discr VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE accessible_employee (accessible_id INTEGER NOT NULL, employee_id INTEGER NOT NULL, PRIMARY KEY(accessible_id, employee_id))');
        $this->addSql('CREATE INDEX IDX_3C3A71EC4FC8BF7 ON accessible_employee (accessible_id)');
        $this->addSql('CREATE INDEX IDX_3C3A71EC8C03F15C ON accessible_employee (employee_id)');
        $this->addSql('CREATE TABLE communication (id INTEGER NOT NULL, type VARCHAR(64) NOT NULL, subject VARCHAR(64) NOT NULL, "from" DATETIME NOT NULL, "to" DATETIME DEFAULT NULL, summary CLOB DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE subject (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, discr VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE person (id INTEGER NOT NULL, name VARCHAR(64) NOT NULL, surname VARCHAR(64) NOT NULL, city VARCHAR(64) DEFAULT NULL, street VARCHAR(64) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, zip VARCHAR(16) DEFAULT NULL, country VARCHAR(64) DEFAULT NULL, phone VARCHAR(32) DEFAULT NULL, email VARCHAR(64) DEFAULT NULL, description CLOB DEFAULT NULL, photo VARCHAR(128) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE employee (id INTEGER NOT NULL, username VARCHAR(128) NOT NULL, password VARCHAR(255) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , wage VARCHAR(32) DEFAULT NULL, account VARCHAR(64) DEFAULT NULL, hired DATE DEFAULT NULL, fired DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D9F75A1F85E0677 ON employee (username)');
        $this->addSql('CREATE TABLE participation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, communication_id INTEGER NOT NULL, person_id INTEGER NOT NULL, role VARCHAR(64) DEFAULT NULL)');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C2D1E0C ON participation (communication_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F217BBB47 ON participation (person_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE accessible');
        $this->addSql('DROP TABLE accessible_employee');
        $this->addSql('DROP TABLE communication');
        $this->addSql('DROP TABLE subject');
        $this->addSql('DROP TABLE person');
        $this->addSql('DROP TABLE employee');
        $this->addSql('DROP TABLE participation');
    }
}
