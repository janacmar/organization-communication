<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200508095613 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_3C3A71EC8C03F15C');
        $this->addSql('DROP INDEX IDX_3C3A71EC4FC8BF7');
        $this->addSql('CREATE TEMPORARY TABLE __temp__accessible_employee AS SELECT accessible_id, employee_id FROM accessible_employee');
        $this->addSql('DROP TABLE accessible_employee');
        $this->addSql('CREATE TABLE accessible_employee (accessible_id INTEGER NOT NULL, employee_id INTEGER NOT NULL, PRIMARY KEY(accessible_id, employee_id), CONSTRAINT FK_3C3A71EC4FC8BF7 FOREIGN KEY (accessible_id) REFERENCES accessible (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_3C3A71EC8C03F15C FOREIGN KEY (employee_id) REFERENCES employee (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO accessible_employee (accessible_id, employee_id) SELECT accessible_id, employee_id FROM __temp__accessible_employee');
        $this->addSql('DROP TABLE __temp__accessible_employee');
        $this->addSql('CREATE INDEX IDX_3C3A71EC8C03F15C ON accessible_employee (employee_id)');
        $this->addSql('CREATE INDEX IDX_3C3A71EC4FC8BF7 ON accessible_employee (accessible_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__communication AS SELECT id, type, subject, "from", "to", summary FROM communication');
        $this->addSql('DROP TABLE communication');
        $this->addSql('CREATE TABLE communication (id INTEGER NOT NULL, type VARCHAR(64) NOT NULL COLLATE BINARY, subject VARCHAR(64) NOT NULL COLLATE BINARY, "from" DATETIME NOT NULL, "to" DATETIME DEFAULT NULL, summary CLOB DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_F9AFB5EBBF396750 FOREIGN KEY (id) REFERENCES accessible (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO communication (id, type, subject, "from", "to", summary) SELECT id, type, subject, "from", "to", summary FROM __temp__communication');
        $this->addSql('DROP TABLE __temp__communication');
        $this->addSql('CREATE TEMPORARY TABLE __temp__person AS SELECT id, name, surname, city, street, house, zip, country, phone, email, description, photo FROM person');
        $this->addSql('DROP TABLE person');
        $this->addSql('CREATE TABLE person (id INTEGER NOT NULL, name VARCHAR(64) NOT NULL COLLATE BINARY, surname VARCHAR(64) NOT NULL COLLATE BINARY, city VARCHAR(64) DEFAULT NULL COLLATE BINARY, street VARCHAR(64) DEFAULT NULL COLLATE BINARY, house VARCHAR(16) DEFAULT NULL COLLATE BINARY, zip VARCHAR(16) DEFAULT NULL COLLATE BINARY, country VARCHAR(64) DEFAULT NULL COLLATE BINARY, phone VARCHAR(32) DEFAULT NULL COLLATE BINARY, email VARCHAR(64) DEFAULT NULL COLLATE BINARY, description CLOB DEFAULT NULL COLLATE BINARY, photo VARCHAR(128) DEFAULT NULL COLLATE BINARY, PRIMARY KEY(id), CONSTRAINT FK_34DCD176BF396750 FOREIGN KEY (id) REFERENCES subject (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO person (id, name, surname, city, street, house, zip, country, phone, email, description, photo) SELECT id, name, surname, city, street, house, zip, country, phone, email, description, photo FROM __temp__person');
        $this->addSql('DROP TABLE __temp__person');
        $this->addSql('DROP INDEX UNIQ_5D9F75A1F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__employee AS SELECT id, username, password, roles, wage, account, hired, fired FROM employee');
        $this->addSql('DROP TABLE employee');
        $this->addSql('CREATE TABLE employee (id INTEGER NOT NULL, username VARCHAR(128) NOT NULL COLLATE BINARY, password VARCHAR(255) NOT NULL COLLATE BINARY, roles CLOB NOT NULL COLLATE BINARY --(DC2Type:json)
        , wage VARCHAR(32) DEFAULT NULL COLLATE BINARY, account VARCHAR(64) DEFAULT NULL COLLATE BINARY, hired DATE DEFAULT NULL, fired DATE DEFAULT NULL, PRIMARY KEY(id), CONSTRAINT FK_5D9F75A1BF396750 FOREIGN KEY (id) REFERENCES subject (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO employee (id, username, password, roles, wage, account, hired, fired) SELECT id, username, password, roles, wage, account, hired, fired FROM __temp__employee');
        $this->addSql('DROP TABLE __temp__employee');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D9F75A1F85E0677 ON employee (username)');
        $this->addSql('DROP INDEX IDX_AB55E24F217BBB47');
        $this->addSql('DROP INDEX IDX_AB55E24F1C2D1E0C');
        $this->addSql('CREATE TEMPORARY TABLE __temp__participation AS SELECT id, communication_id, person_id, role FROM participation');
        $this->addSql('DROP TABLE participation');
        $this->addSql('CREATE TABLE participation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, communication_id INTEGER NOT NULL, person_id INTEGER NOT NULL, role VARCHAR(64) DEFAULT NULL COLLATE BINARY, CONSTRAINT FK_AB55E24F1C2D1E0C FOREIGN KEY (communication_id) REFERENCES communication (id) NOT DEFERRABLE INITIALLY IMMEDIATE, CONSTRAINT FK_AB55E24F217BBB47 FOREIGN KEY (person_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE)');
        $this->addSql('INSERT INTO participation (id, communication_id, person_id, role) SELECT id, communication_id, person_id, role FROM __temp__participation');
        $this->addSql('DROP TABLE __temp__participation');
        $this->addSql('CREATE INDEX IDX_AB55E24F217BBB47 ON participation (person_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C2D1E0C ON participation (communication_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP INDEX IDX_3C3A71EC4FC8BF7');
        $this->addSql('DROP INDEX IDX_3C3A71EC8C03F15C');
        $this->addSql('CREATE TEMPORARY TABLE __temp__accessible_employee AS SELECT accessible_id, employee_id FROM accessible_employee');
        $this->addSql('DROP TABLE accessible_employee');
        $this->addSql('CREATE TABLE accessible_employee (accessible_id INTEGER NOT NULL, employee_id INTEGER NOT NULL, PRIMARY KEY(accessible_id, employee_id))');
        $this->addSql('INSERT INTO accessible_employee (accessible_id, employee_id) SELECT accessible_id, employee_id FROM __temp__accessible_employee');
        $this->addSql('DROP TABLE __temp__accessible_employee');
        $this->addSql('CREATE INDEX IDX_3C3A71EC4FC8BF7 ON accessible_employee (accessible_id)');
        $this->addSql('CREATE INDEX IDX_3C3A71EC8C03F15C ON accessible_employee (employee_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__communication AS SELECT id, type, subject, "from", "to", summary FROM communication');
        $this->addSql('DROP TABLE communication');
        $this->addSql('CREATE TABLE communication (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, type VARCHAR(64) NOT NULL, subject VARCHAR(64) NOT NULL, "from" DATETIME NOT NULL, "to" DATETIME DEFAULT NULL, summary CLOB DEFAULT NULL)');
        $this->addSql('INSERT INTO communication (id, type, subject, "from", "to", summary) SELECT id, type, subject, "from", "to", summary FROM __temp__communication');
        $this->addSql('DROP TABLE __temp__communication');
        $this->addSql('DROP INDEX UNIQ_5D9F75A1F85E0677');
        $this->addSql('CREATE TEMPORARY TABLE __temp__employee AS SELECT id, username, password, roles, wage, account, hired, fired FROM employee');
        $this->addSql('DROP TABLE employee');
        $this->addSql('CREATE TABLE employee (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(128) NOT NULL, password VARCHAR(255) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , wage VARCHAR(32) DEFAULT NULL, account VARCHAR(64) DEFAULT NULL, hired DATE DEFAULT NULL, fired DATE DEFAULT NULL)');
        $this->addSql('INSERT INTO employee (id, username, password, roles, wage, account, hired, fired) SELECT id, username, password, roles, wage, account, hired, fired FROM __temp__employee');
        $this->addSql('DROP TABLE __temp__employee');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5D9F75A1F85E0677 ON employee (username)');
        $this->addSql('DROP INDEX IDX_AB55E24F1C2D1E0C');
        $this->addSql('DROP INDEX IDX_AB55E24F217BBB47');
        $this->addSql('CREATE TEMPORARY TABLE __temp__participation AS SELECT id, communication_id, person_id, role FROM participation');
        $this->addSql('DROP TABLE participation');
        $this->addSql('CREATE TABLE participation (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, communication_id INTEGER NOT NULL, person_id INTEGER NOT NULL, role VARCHAR(64) DEFAULT NULL)');
        $this->addSql('INSERT INTO participation (id, communication_id, person_id, role) SELECT id, communication_id, person_id, role FROM __temp__participation');
        $this->addSql('DROP TABLE __temp__participation');
        $this->addSql('CREATE INDEX IDX_AB55E24F1C2D1E0C ON participation (communication_id)');
        $this->addSql('CREATE INDEX IDX_AB55E24F217BBB47 ON participation (person_id)');
        $this->addSql('CREATE TEMPORARY TABLE __temp__person AS SELECT id, name, surname, city, street, house, zip, country, phone, email, description, photo FROM person');
        $this->addSql('DROP TABLE person');
        $this->addSql('CREATE TABLE person (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, name VARCHAR(64) NOT NULL, surname VARCHAR(64) NOT NULL, city VARCHAR(64) DEFAULT NULL, street VARCHAR(64) DEFAULT NULL, house VARCHAR(16) DEFAULT NULL, zip VARCHAR(16) DEFAULT NULL, country VARCHAR(64) DEFAULT NULL, phone VARCHAR(32) DEFAULT NULL, email VARCHAR(64) DEFAULT NULL, description CLOB DEFAULT NULL, photo VARCHAR(128) DEFAULT NULL)');
        $this->addSql('INSERT INTO person (id, name, surname, city, street, house, zip, country, phone, email, description, photo) SELECT id, name, surname, city, street, house, zip, country, phone, email, description, photo FROM __temp__person');
        $this->addSql('DROP TABLE __temp__person');
    }
}
