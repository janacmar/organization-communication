<?php

namespace App\Functionality;

use App\Entity\Communication;
use App\Entity\Employee;
use App\Entity\Participation;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

/** Helper service for communications. */
class CommunicationFunctionality
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @param EntityManagerInterface $em */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Saves communication to database including its participation.
     *
     * @param Communication $communication
     */
    public function save (Communication $communication)
    {
        // participation persisted thanks to cascade={"persist"} in Communication Entity
        $this->em->persist($communication);
        $this->em->flush();
    }

    /**
     * Removes communication from database including its participation.
     *
     * @param Communication $communication
     */
    public function delete (Communication $communication)
    {
        // corresponding accessible_employee table records removed
        foreach ($communication->getEmployees() as $employee) {
            $communication->removeEmployee($employee);
        }
        $this->em->persist($communication);
        $this->em->flush();

        // participation removed thanks to cascade={"remove"} in Communication Entity
        $this->em->remove($communication);
        $this->em->flush();
    }

    /**
     * Sorts employees by their surname and name.
     *
     * @param Employee[] $employees
     * @return Employee[] $employees
     */
    public function sortEmployeesByName ($employees)
    {
        usort($employees, function (Employee $a, Employee $b) {
            if ($a->getSurname() == $b->getSurname()) {
                return strcmp($a->getName(), $b->getName());
            }
            return strcmp($a->getSurname(), $b->getSurname());
        });
        return $employees;
    }

    /**
     * Sorts participations by surname and name of participating persons.
     *
     * @param Collection $participations
     * @return Participation[]
     */
    public function sortParticipationsByName ($participations)
    {
        $participations = $participations->toArray();
        usort($participations, function (Participation $a, Participation $b) {
            if ($a->getPerson()->getSurname() == $b->getPerson()->getSurname()) {
                return strcmp($a->getPerson()->getName(), $b->getPerson()->getName());
            }
            return strcmp($a->getPerson()->getSurname(), $b->getPerson()->getSurname());
        });
        return $participations;
    }

    /**
     * Returns only participations of external persons (persons who are not employees).
     *
     * @param Participation[] $participations
     * @return Participation[]
     */
    public function externalPersonFilter ($participations)
    {
        return array_filter($participations, function (Participation $participation) {
            return ! ($participation->getPerson() instanceof Employee);
        });
    }

    /**
     * Returns only participations of employees.
     *
     * @param Participation[] $participations
     * @return Participation[]
     */
    public function employeeFilter ($participations)
    {
        return array_filter($participations, function (Participation $participation) {
            return $participation->getPerson() instanceof Employee;
        });
    }

    /**
     * Returns all employees authorized to communication.
     *
     * @param Communication $communication
     * @return Employee[] $employees
     */
    public function getAllAuthorizedEmployees (Communication $communication)
    {
        $authorizedEmployees = $communication->getEmployees()->toArray();
        // adds employees participating in communication to $authorizedEmployees
        $employeeParticipations = $this->employeeFilter($communication->getParticipations()->toArray());
        foreach ($employeeParticipations as $participation) {
            if (! in_array($participation->getPerson(), $authorizedEmployees)) {
                array_push($authorizedEmployees, $participation->getPerson());
            }
        }

        /** @var Employee[] $allEmployees */
        $allEmployees = $this->em->getRepository(Employee::class)->findAll();

        // adds communication watchers and administrators to $authorizedEmployees
        foreach ($allEmployees as $employee) {
            if ((in_array('ROLE_COMMUNICATION_WATCHER', $employee->getRoles()) or
                 in_array('ROLE_ADMINISTRATOR', $employee->getRoles())) and !
                in_array($employee, $authorizedEmployees)) {
                array_push($authorizedEmployees, $employee);
            }
        }
        return $authorizedEmployees;
    }
}