<?php

namespace App\Functionality;

use App\Entity\Person;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

/** Helper service for external persons (persons who are not employees). */
class ExternalPersonFunctionality
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var Filesystem */
    protected $filesystem;

    /**
     * @param EntityManagerInterface $em
     * @param Filesystem $filesystem
     */
    public function __construct(EntityManagerInterface $em, Filesystem $filesystem)
    {
        $this->em = $em;
        $this->filesystem = $filesystem;
    }

    /**
     * Saves external person to database including its participation in communications.
     *
     * @param Person $person
     */
    public function save (Person $person)
    {
        // participation persisted thanks to cascade={"persist"} in Person Entity
        $this->em->persist($person);
        $this->em->flush();
    }

    /**
     * Removes external person from database including its participation in communications.
     *
     * @param Person $person
     */
    public function delete (Person $person)
    {
        if ($person->getPhoto()) {
            $this->filesystem->remove('images/'.$person->getPhoto());
        }
        // participation removed thanks to cascade={"remove"} in Person Entity
        $this->em->remove($person);
        $this->em->flush();
    }
}