<?php

namespace App\Functionality;

use App\Entity\Employee;
use App\Entity\Participation;
use App\Entity\Person;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Security;

/** Helper service common for all person subtypes. */
class PersonFunctionality
{
    /** @var Filesystem */
    protected $filesystem;

    /** @var Security */
    protected $security;

    /** Defined in services.yaml. */
    protected $targetDirectory;

    /**
     * @param Filesystem $filesystem
     * @param Security $security
     * @param $targetDirectory
     */
    public function __construct(Filesystem $filesystem, Security $security, $targetDirectory)
    {
        $this->filesystem = $filesystem;
        $this->security = $security;
        $this->targetDirectory = $targetDirectory;
    }

    /**
     * Uploads person's photo (if given) in images directory (replaces current photo if it exists).
     *
     * @param UploadedFile|null $file
     * @param Person $person
     */
    public function uploadPersonPhoto (?UploadedFile $file, Person $person)
    {
        if ($file) {
            if ($person->getPhoto()) {
                $this->filesystem->remove('images/'.$person->getPhoto());
            }
            $fileName = uniqid().'.'.$file->guessExtension();
            $file->move($this->targetDirectory, $fileName);
            $person->setPhoto($fileName);
        }
    }

    /**
     * Sorts participations by end (first those without end) and beginning of communications.
     *
     * @param Participation[] $participations
     * @return Participation[] $participations
     */
    public function sortParticipationsByDate ($participations)
    {
        usort($participations, function (Participation $a, Participation $b) {
            if ($a->getCommunication()->getTo() == $b->getCommunication()->getTo()) {
                if ($a->getCommunication()->getFrom() < $b->getCommunication()->getFrom()) {
                    return 1;
                }
                if ($a->getCommunication()->getFrom() > $b->getCommunication()->getFrom()) {
                    return -1;
                }
                else return 0;
            }
            if (($a->getCommunication()->getTo() < $b->getCommunication()->getTo() and
                 $a->getCommunication()->getTo() != null) or
                $b->getCommunication()->getTo() == null) {
                return 1;
            }
            else return -1;
        });
        return $participations;
    }

    /**
     * Returns only external persons (persons who are not employees).
     *
     * @param object[] $persons
     * @return Person[]
     */
    public function externalPersonFilter ($persons)
    {
        return array_filter($persons, function (Person $person) {
            return ! ($person instanceof Employee);
        });
    }

    /**
     * Returns only particpations in communications accessible by user.
     *
     * @param Collection $participations
     * @return Participation[]
     */
    public function accessibleParticipationFilter ($participations)
    {
        $participations = $participations->toArray();
        if ($this->security->isGranted('ROLE_COMMUNICATION_WATCHER')) {
            return $participations;
        }
        return array_filter($participations, function (Participation $participation) {
            return $this->security->isGranted('access', $participation->getCommunication());
        });
    }
}