<?php

namespace App\Functionality;

use App\Entity\Employee;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Security;

/** Helper service for employees. */
class EmployeeFunctionality
{
    /** @var EntityManagerInterface */
    protected $em;

    /** @var Security */
    protected $security;

    /** @var Filesystem */
    protected $filesystem;

    /** @var UserPasswordEncoderInterface */
    protected $passwordEncoder;

    /**
     * @param EntityManagerInterface $em
     * @param Security $security
     * @param Filesystem $filesystem
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $em, Security $security, Filesystem $filesystem, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->em = $em;
        $this->security = $security;
        $this->filesystem = $filesystem;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Saves employee to database including his participation in communications.
     *
     * @param Employee $employee
     */
    public function save (Employee $employee)
    {
        if ($this->security->isGranted('ROLE_EMPLOYEE_MANAGER')) {
            $employee->setPassword($this->passwordEncoder->encodePassword($employee, $employee->getPassword()));
        }
        // participation persisted thanks to cascade={"persist"} in Person Entity
        $this->em->persist($employee);
        $this->em->flush();
    }

    /**
     * Removes employee from database including his participation in communications.
     *
     * @param Employee $employee
     */
    public function delete (Employee $employee)
    {
        if ($employee->getPhoto()) {
            $this->filesystem->remove('images/'.$employee->getPhoto());
        }
        // corresponding accessible_employee table records removed
        foreach ($employee->getAccessibles() as $accessible) {
            $employee->removeAccessible($accessible);
        }
        $this->em->persist($employee);
        $this->em->flush();

        // participation removed thanks to cascade={"remove"} in Person Entity
        $this->em->remove($employee);
        $this->em->flush();
    }
}