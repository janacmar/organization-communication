<?php

namespace App\Functionality;

use App\Entity\Communication;
use Symfony\Component\Security\Core\Security;

/** General helper service common for all. */
class CommonFunctionality
{
    /** @var Security */
    protected $security;

    /** @param Security $security */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * Sorts communications by their end (first those without end) and beginning.
     *
     * @param object[] $communications
     * @return object[] $communications
     */
    public function sortCommunicationsByDate ($communications)
    {
        usort($communications, function (Communication $a, Communication $b) {
            if ($a->getTo() == $b->getTo()) {
                if ($a->getFrom() < $b->getFrom()) {
                    return 1;
                }
                if ($a->getFrom() > $b->getFrom()) {
                    return -1;
                }
                else return 0;
            }
            if (($a->getTo() < $b->getTo() and $a->getTo() != null) or $b->getTo() == null) {
                return 1;
            }
            else return -1;
        });
        return $communications;
    }

    /**
     * Returns only communications accessible by user.
     *
     * @param object[] $communications
     * @return object[]
     */
    public function accessibleCommunicationFilter ($communications)
    {
        if ($this->security->isGranted('ROLE_COMMUNICATION_WATCHER')) {
            return $communications;
        }
        return array_filter($communications, function (Communication $communication) {
            return $this->security->isGranted('access', $communication);
        });
    }
}