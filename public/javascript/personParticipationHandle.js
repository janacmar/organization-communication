/* Used to adding and removing person's participations in communications. */

let $collectionHolder;
let $addParticipationButton = $('<button type="button" class="add_participation_link">Add communication</button>');

jQuery(document).ready(function() {
    // participation container
    $collectionHolder = $('#person_participations');
    // adds delete button to all participations
    $collectionHolder.find('div[id]').each(function() {
        addParticipationFormDeleteLink($(this));
    });
    $collectionHolder.append($addParticipationButton);
    // sets index as number of participations
    $collectionHolder.data('index', $collectionHolder.find('input').length);
    $addParticipationButton.on('click', function() {
        addParticipationForm($collectionHolder, $addParticipationButton);
    });
});

/* Adds new participation form before add button in participation container. */
function addParticipationForm($collectionHolder, $addParticipationButton) {
    let prototype = $collectionHolder.data('prototype');
    let index = $collectionHolder.data('index');
    let newForm = prototype;
    newForm = newForm.replace(/__name__/g, index);
    $collectionHolder.data('index', index + 1);
    let $newForm = $(newForm);
    $addParticipationButton.before($newForm);
    addParticipationFormDeleteLink($newForm);
}

/* Adds delete button at the end of participation form. */
function addParticipationFormDeleteLink($participationForm) {
    let $removeFormButton = $('<button type="button">Remove communication</button>');
    $participationForm.append($removeFormButton);
    $removeFormButton.on('click', function() {
        // participation removed from database thanks to orphanRemoval=true in Person Entity
        $participationForm.remove();
    });
}