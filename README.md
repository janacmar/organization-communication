18.5.2020

Copyright (C) 2020 Martin Janáček – All rights reserved

# Organization Communication

This project is part of Martin Janáček's (mjmartinmj@gmail.com) bachelor thesis at CTU in Prague, Czech Republic.
The project contains a web application (created in Symfony) serving organizations to record and manage effectively
their communications with other subjects and within themselves. It's not a complete application but a mere prototype
demonstrating basic functionalities of the designed system.

**Installation Guide**

PHP 7.2.10 or higher is required with following extensions enabled: fileinfo, pdo\_sqlite. To run the application,
download the project folder in your computer. Open the command line and enter the bin folder in the project folder.
Then run the command: *php console server:run*. Open the displayed URL address in your web browser and you will see
the login page. You can end the application by pressing Ctrl+C in the command line.

**User Guide**

There are three main entities managed in the application: employees (members of an organization using the software),
external persons and communications among former two. For each of these it is possible to display the list of all
(in the menu) and profiles of individuals (in lists and other profiles containing them), to edit and delete individuals
(in their profiles) and to create new ones (in corresponding lists of all). It is also possible to display the list
of communications a specific person (external or employee) participated in (in its profile).

One must always log in to access the application. Then user can display in the menu the list of communications he
participated in. But not every user can do everything (users correspond to employees). There are several prepared
users with specific functions:

- **smimar** (administrator) is authorized to everything,
- **doejoh** (communication watcher) can access all communications,
- **blahen** (employee manager) can create and delete (except himself) employees, edit their properties (common
  users can edit only communication participation) and view their wages and bank accounts (common user sees only his
  own wage and account),
- **redjos** is common user.

All passwords are preset to 1234. It's not possible to edit user functions. User must be authorized to a communication
to access it in any way. User can manually change another user's authorization to a communication (in its edit
page). But those participating in a communication and those with communication watcher function will be authorized
to it automatically. It is possible to display the list of employees authorized to a specific communication (in its
profile). User must log out (in the menu) so that another user can log in.